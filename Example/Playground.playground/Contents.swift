//: Playground - noun: a place where people can play

import UIKit
import MurmurHash3

var str = "Hello, playground"

MurmurHash3.hash32(key: str)
MurmurHash3.hash128(key: str).hexString

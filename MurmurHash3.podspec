#
# Be sure to run `pod lib lint MurmurHash3.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'MurmurHash3'
  s.version          = '0.2.0'
  s.summary          = 'MurmurHash3 non-cryptographic hash function'

  s.description      = <<-DESC
MurmurHash is a non-cryptographic hash function suitable for general hash-based
lookup. This project implements MurmurHash3 x64 32-bit and 128-bit functions.
                       DESC

  s.homepage         = 'https://gitlab.com/shadone/MurmurHash3'
  s.license          = { :type => 'Apache', :file => 'LICENSE' }
  s.author           = { 'Denis Dzyubenko' => 'denis@ddenis.info' }
  s.source           = { :git => 'https://gitlab.com/shadone/MurmurHash3.git', :tag => s.version.to_s }
  s.swift_version    = '5.0'

  s.ios.deployment_target = '8.0'

  s.source_files = 'Sources/MurmurHash3/**/*'
end

# MurmurHash3

[![Version](https://img.shields.io/cocoapods/v/MurmurHash3.svg?style=flat)](https://cocoapods.org/pods/MurmurHash3)
[![License](https://img.shields.io/cocoapods/l/MurmurHash3.svg?style=flat)](LICENSE)
[![Platform](https://img.shields.io/cocoapods/p/MurmurHash3.svg?style=flat)](https://cocoapods.org/pods/MurmurHash3)

[MurmurHash3](https://en.wikipedia.org/wiki/MurmurHash) x64 32-bit and 128-bit
implementation in Swift.

## Example

```swift
    import MurmurHash3

    var str = "Hello, playground"

    MurmurHash3.hash32(key: str) // 888929604
    MurmurHash3.hash128(key: str).hexString // "a21b5a555e5be42fa42cacb9c4241a1f"
```

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

### SwiftPM

1. Add the following to your `Package.swift`:
    ```swift
    .package(url: "https://gitlab.com/shadone/MurmurHash3.git", .upToNextMajor(from: "1.0")),
    ```
2. Add `MurmurHash3` to your app target dependencies:
    ```swift
    .target(name: "App", dependencies: ["MurmurHash3"]),
    ```
3. Open your project in Xcode.

### CocoaPods

MurmurHash3 is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'MurmurHash3'
```

## Author

Denis Dzyubenko, denis@ddenis.info

## License

The code is based on implementation by [Joao
Pedrosa](https://github.com/jpedrosa/sua/blob/master/Sources/murmurhash3.swift)

MurmurHash3 is available under the Apache license. See the LICENSE file for more info.

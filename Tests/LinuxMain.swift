import XCTest

import MurmurHash3Tests

var tests = [XCTestCaseEntry]()
tests += MurmurHash3Tests.allTests()
XCTMain(tests)

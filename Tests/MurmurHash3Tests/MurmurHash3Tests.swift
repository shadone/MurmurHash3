import XCTest
@testable import MurmurHash3

final class MurmurHash3Tests: XCTestCase {
  func testHash32() {
    XCTAssertEqual(MurmurHash3.hash32(key: "kinkajou"), 3067714808)
    XCTAssertEqual(MurmurHash3.hash32(key: "kinkajoukinkajou"), 1819926224)
  }
  
  func testHash32CChar() {
    let bytes: [CChar] = "kinkajou".cString(using: .utf8)!
    XCTAssertEqual(MurmurHash3.hash32CChar(key: bytes, maxBytes: bytes.count-1), 3067714808)
  }
  
  func testHash32CompareWithJS() {
    // From https://github.com/pid/murmurHash3js/blob/master/test/test-server.js
    XCTAssertEqual(MurmurHash3.hash32(key: "I will not buy this record, it is scratched."), 2832214938)
    XCTAssertEqual(MurmurHash3.hash32(key: "My hovercraft is full of eels.", seed: 0), 2953494853)
    XCTAssertEqual(MurmurHash3.hash32(key: "My hovercraft is full of eels.", seed: 25), 2520298415)
    XCTAssertEqual(MurmurHash3.hash32(key: "My hovercraft is full of eels.", seed: 128), 2204470254)
    XCTAssertEqual(MurmurHash3.hash32(key: "I will not buy this record, it is scratched."), 2832214938)
    XCTAssertEqual(MurmurHash3.hash32(key: ""), 0)
    XCTAssertEqual(MurmurHash3.hash32(key: "0"), 3530670207)
    XCTAssertEqual(MurmurHash3.hash32(key: "01"), 1642882560)
    XCTAssertEqual(MurmurHash3.hash32(key: "012"), 3966566284)
    XCTAssertEqual(MurmurHash3.hash32(key: "0123"), 3558446240)
    XCTAssertEqual(MurmurHash3.hash32(key: "01234"), 433070448)
    XCTAssertEqual(MurmurHash3.hash32(key: "", seed: 1), 1364076727)
  }
  
  func testHash128() {
    XCTAssertEqual(MurmurHash3.hash128(key: "kinkajou").hexString, "f748ce75ec508ae6425b3730949a3ee5")
    XCTAssertEqual(MurmurHash3.hash128(key: "kinkajoukinkajou").hexString, "9f2a91626c6a75774efab1b1830674f4")
  }
  
  func testHash128CompareWithGo() {
    // From https://github.com/spaolacci/murmur3/blob/master/murmur_test.go
    XCTAssertEqual(MurmurHash3.hash128(key: "").hexString, "00000000000000000000000000000000")
    XCTAssertEqual(MurmurHash3.hash128(key: "hello").hexString, "cbd8a7b341bd9b025b1e906a48ae1d19")
    XCTAssertEqual(MurmurHash3.hash128(key: "hello, world").hexString, "342fac623a5ebc8e4cdcbc079642414d")
    XCTAssertEqual(MurmurHash3.hash128(key: "19 Jan 2038 at 3:14:07 AM").hexString, "b89e5988b737affc664fc2950231b2cb")
    XCTAssertEqual(MurmurHash3.hash128(key: "The quick brown fox jumps over the lazy dog.").hexString, "cd99481f9ee902c9695da1a38987b6e7")
    
    XCTAssertEqual(MurmurHash3.hash128(key: "", seed: 1).hexString, "4610abe56eff5cb551622daa78f83583")
    XCTAssertEqual(MurmurHash3.hash128(key: "hello", seed: 1).hexString, "a78ddff5adae8d10128900ef20900135")
    XCTAssertEqual(MurmurHash3.hash128(key: "hello, world", seed: 1).hexString, "8b95f808840725c61597ed5422bd493b")
    XCTAssertEqual(MurmurHash3.hash128(key: "19 Jan 2038 at 3:14:07 AM", seed: 1).hexString, "2a929de9c8f97b2f56a41d99af43a2db")
    XCTAssertEqual(MurmurHash3.hash128(key: "The quick brown fox jumps over the lazy dog.", seed: 1).hexString, "fb3325171f9744daaaf8b92a5f722952")
    
    XCTAssertEqual(MurmurHash3.hash128(key: "", seed: 0x2a).hexString, "f02aa77dfa1b8523d1016610da11cbb9")
    XCTAssertEqual(MurmurHash3.hash128(key: "hello", seed: 0x2a).hexString, "c4b8b3c960af6f082334b875b0efbc7a")
    XCTAssertEqual(MurmurHash3.hash128(key: "hello, world", seed: 0x2a).hexString, "b91864d797caa956d5d139a55afe6150")
    XCTAssertEqual(MurmurHash3.hash128(key: "19 Jan 2038 at 3:14:07 AM", seed: 0x2a).hexString, "fd8f19ebdc8c6b6ad30fdc310fa08ff9")
    XCTAssertEqual(MurmurHash3.hash128(key: "The quick brown fox jumps over the lazy dog.", seed: 0x2a).hexString, "74f33c659cda5af74ec7a891caf316f0")
  }
  
  func testHash128CompareWithJS() {
    // From https://github.com/pid/murmurHash3js/blob/master/test/test-server.js
    XCTAssertEqual(MurmurHash3.hash128(key: "I will not buy this record, it is scratched.").hexString, "c382657f9a06c49d4a71fdc6d9b0d48f")
    XCTAssertEqual(MurmurHash3.hash128(key: "").hexString, "00000000000000000000000000000000")
    XCTAssertEqual(MurmurHash3.hash128(key: "0").hexString, "2ac9debed546a3803a8de9e53c875e09")
    XCTAssertEqual(MurmurHash3.hash128(key: "01").hexString, "649e4eaa7fc1708ee6945110230f2ad6")
    XCTAssertEqual(MurmurHash3.hash128(key: "012").hexString, "ce68f60d7c353bdb00364cd5936bf18a")
    XCTAssertEqual(MurmurHash3.hash128(key: "0123").hexString, "0f95757ce7f38254b4c67c9e6f12ab4b")
    XCTAssertEqual(MurmurHash3.hash128(key: "01234").hexString, "0f04e459497f3fc1eccc6223a28dd613")
    XCTAssertEqual(MurmurHash3.hash128(key: "012345").hexString, "88c0a92586be0a2781062d6137728244")
    XCTAssertEqual(MurmurHash3.hash128(key: "0123456").hexString, "13eb9fb82606f7a6b4ebef492fdef34e")
    XCTAssertEqual(MurmurHash3.hash128(key: "01234567").hexString, "8236039b7387354dc3369387d8964920")
    XCTAssertEqual(MurmurHash3.hash128(key: "012345678").hexString, "4c1e87519fe738ba72a17af899d597f1")
    XCTAssertEqual(MurmurHash3.hash128(key: "0123456789").hexString, "3f9652ac3effeb248027a17cf2990b07")
    XCTAssertEqual(MurmurHash3.hash128(key: "0123456789a").hexString, "4bc3eacd29d386297cb2d9e797da9c92")
    XCTAssertEqual(MurmurHash3.hash128(key: "0123456789ab").hexString, "66352b8cee9e3ca7a9edf0b381a8fc58")
    XCTAssertEqual(MurmurHash3.hash128(key: "0123456789abc").hexString, "5eb2f8db4265931e801ce853e61d0ab7")
    XCTAssertEqual(MurmurHash3.hash128(key: "0123456789abcd").hexString, "07a4a014dd59f71aaaf437854cd22231")
    XCTAssertEqual(MurmurHash3.hash128(key: "0123456789abcde").hexString, "a62dd5f6c0bf23514fccf50c7c544cf0")
    XCTAssertEqual(MurmurHash3.hash128(key: "0123456789abcdef").hexString, "4be06d94cf4ad1a787c35b5c63a708da")
    XCTAssertEqual(MurmurHash3.hash128(key: "", seed: 1).hexString, "4610abe56eff5cb551622daa78f83583")
  }
  
  static var allTests = [
    ("testHash32", testHash32),
    ("testHash32CChar", testHash32CChar),
    ("testHash32CompareWithJS", testHash32CompareWithJS),
    ("testHash128CompareWithGo", testHash128CompareWithGo),
    ("testHash128CompareWithJS", testHash128CompareWithJS),
  ]
}

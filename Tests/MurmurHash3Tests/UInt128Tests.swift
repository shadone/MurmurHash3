import XCTest
@testable import MurmurHash3

final class UInt128Tests: XCTestCase {
  func testTiny() {
    let n = UInt128(0, 0xFE)
    XCTAssertEqual(n.h1, 0)
    XCTAssertEqual(n.h2, 0xFE)
    XCTAssertEqual(n.hexString, "000000000000000000000000000000fe")
  }
  
  func testSmall() {
    let n = UInt128(0, UInt64.max)
    XCTAssertEqual(n.h1, 0)
    XCTAssertEqual(n.h2, UInt64.max)
    XCTAssertEqual(n.hexString, "0000000000000000ffffffffffffffff")
  }
  
  func testLarge() {
    let n = UInt128(UInt64.max, 0)
    XCTAssertEqual(n.h1, UInt64.max)
    XCTAssertEqual(n.h2, 0)
    XCTAssertEqual(n.hexString, "ffffffffffffffff0000000000000000")
  }
  
  func testHuge() {
    let n = UInt128(UInt64.max, UInt64.max)
    XCTAssertEqual(n.h1, UInt64.max)
    XCTAssertEqual(n.h2, UInt64.max)
    XCTAssertEqual(n.hexString, "ffffffffffffffffffffffffffffffff")
  }
  
  static var allTests = [
    ("testTiny", testTiny),
    ("testSmall", testSmall),
    ("testLarge", testLarge),
    ("testHuge", testHuge),
  ]
}

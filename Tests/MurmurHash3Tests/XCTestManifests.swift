import XCTest

#if !canImport(ObjectiveC)
public func allTests() -> [XCTestCaseEntry] {
    return [
        testCase(MurmurHash3Tests.allTests),
        testCase(UInt128Tests.allTests),
    ]
}
#endif
